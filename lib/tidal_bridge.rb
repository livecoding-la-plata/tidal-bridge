require "em-websocket"
require "json"
require "open3"
require "securerandom"
require "webrick"
require "webrick/https"
require "openssl"
require "byebug" if ENV["DEBUG"]


module TidalBridge
  SSL_KEY_FILE_NAME = "./hydra/hydra-server/certs/key.pem"
  SSL_CERT_FILE_NAME = "./hydra/hydra-server/certs/certificate.pem"
  SSL_KEY = OpenSSL::PKey::RSA.new File.read "./hydra/hydra-server/certs/key.pem"
  SSL_CERT = OpenSSL::X509::Certificate.new File.read "./hydra/hydra-server/certs/certificate.pem"
  
  class Server
    attr_reader :connections, :core, :command, :apps, :core, :tidals

    def initialize(command:)
      @command = command
      @connections = {}
      @apps = {}
      @core = nil
      @tidals = {}
    end

    def run
      Thread.new do
        log "starting webserver"
        root = File.expand_path "./public"

        server = WEBrick::HTTPServer.new(Port: 9292,
                                         SSLEnable: true,
                                         SSLCertificate: SSL_CERT,
                                         SSLPrivateKey: SSL_KEY,
                                         DocumentRoot: root)

        server.start
      end

      sleep 1

      EM.run do
        log "listening on wss://0.0.0.0:8080"

        EM::WebSocket.start host: "0.0.0.0", port: "8080" do |ws|
          ws.onopen do |handshake|
            type, name = *handshake.path.split("/").last(2)
            ws.close if type.nil? || name.nil? || type.empty? || name.empty?

            case type
            when "coder"
              connect_coder(name, ws)
            when "app"
              connect_app(name, ws)
            when "core"
              connect_core(name, ws)
            else
              log "Invalid type: #{type}. Closing connection."
              ws.close
            end

            log "connected #{type} #{name}"
          end

          ws.onclose do
            conn = connections.delete(ws)
            log "client disconnected: #{conn&.name}"
          end

          ws.onmessage do |message|
            conn = connections[ws]

            log "conn: #{conn.id}, received: #{message.inspect}"

            msg = JSON.parse(message)
            case msg["type"]
            when "chat"
              broadcast msg
            when "command"
              process_command msg
            when "code"
              # ask the connection to process the message
              conn.process_message(msg)
              # forward the code hydra
              core.publish(msg.to_json) if core
            else
              log "unknown message type #{msg["type"]}"
            end
          end

          ws.onerror { |e| puts e&.message, e&.backtrace&.join("\n") }
        end
      end
    end

    private

    def connect_app(name, ws)
      log "connecting app #{name}: TODO"
      connections[ws] = Connection.new(name: name, ws: ws)
    end

    def connect_core(name, ws)
      log "connecting CORE (#{name})"
      @core = Connection.new(name: name, ws: ws)
    end

    def connect_coder(name, ws)
      log "connecting coder [#{name}]"
      launch_tidal(name, ws)
      connections[ws] = Coder.new(name: name, ws: ws, tidal: tidals[name])
    end

    def launch_tidal(name, ws)
      tidals[name] ||= Tidal.new command: command
      tidals[name].tap do |t|
        t.on_err = ->(line) { ws.send({type: "code", data: line.chomp}.to_json) }
        t.on_out = ->(line) { ws.send({type: "code", data: line.chomp}.to_json) }
        t.run unless t.running?
      end
    end

    def broadcast(msg)
      puts "broadcasting #{msg}"
      connections.each { |ws, c| c.publish(msg.to_json) }
    end

    def process_command(msg)
        puts "processing #{msg}"

        cmd = msg["data"]["command"]
        case cmd
        when "sendSpy"
            spied = "-Spying on @" + msg["name"] + "\n\n\t"
            code = msg["data"]["code"].gsub("\n","\n\t")
            jsontosend = {'type' => 'sendSpy', 'data' => spied + code}
            connections.each { |ws, c|
                if c.name == msg["data"]["to"] then
                   ws.send(jsontosend.to_json);
                end
            }
        when "spy"
            jsontosend = {'type' => 'spy', 'data' => msg["name"]}
            connections.each { |ws, c|
                if c.name == msg["data"]["coder"] then
                   ws.send(jsontosend.to_json);
                end
            }
        when "coders"
            coders = "-Coders:\n"
            connections.each { |ws, c| coders = coders + "\t\#" + c.name + "\n" };
            jsontosend = {'type' => 'chat', 'data' => coders}
            connections.each { |ws, c|
                if c.name == msg["name"] then
                   ws.send(jsontosend.to_json);
                end
            }
        when "mess"
            jsontosend = {'type' => 'mess', 'data' => {
                'from': msg["name"],
                'block': ("\t" + msg["data"]["block"].gsub("\n","\n\t"))
            }}
            connections.each { |ws, c|
                if c.name == msg["data"]["coder"] then
                   ws.send(jsontosend.to_json);
                   c.process_message({"data" => msg["data"]["block"].gsub("\n"," ").gsub("\t"," ")})
                end
            }
        else
            log "unknown command #{cmd}"
        end
    end

    def log(s)
      puts "[WS] #{s}"
    end
  end

  class Tidal
    attr_reader :command, :pid, :input_queue
    attr_accessor :on_out, :on_err

    def initialize(command: "ghci")
      @pid = nil
      @command = command
      @input_queue = Queue.new
      @on_out = ->(line){ log line }
      @on_err = ->(line){ log line }
    end

    def run
      @running = true
      Thread.new do
        threads = []

        Open3.popen3(command) do |stdin, stdout, stderr, wait_thread|
          @pid = wait_thread.pid

          log "Starting process #{pid}..."

          threads << Thread.new do |t|
            while line = input_queue.pop
              log "stdin: #{line.inspect}"
              stdin.puts line
            end
          end

          threads << Thread.new(stdout) do |t|
            while line = t.gets
              log "stdout: #{line}"
              on_out.call line
            end
          end

          threads << Thread.new(stderr) do |t|
            while line = t.gets
              log "stderr: #{line}"
              on_err.call line
            end
          end

          setup

          threads.each(&:join)
          @running = false
        end
      end
    end

    def running?
      @running
    end

    def evaluate(expression)
      input_queue.push expression
    end

    private

    def setup(file = ENV["TIDAL_SETUP"] || "./setup.hs")
      hs = File.read(file)
      input_queue.push hs
    end

    def log(s)
      puts "[tidal-#{pid}] #{s}"
    end
  end

  class Connection
    attr_reader :name, :id, :ws

    def initialize(name:, ws:)
      @name = name
      @ws = ws
      @id = SecureRandom.uuid
    end

    def process_message(message)
      raise "Subclass Responsibility"
    end

    def publish(msg)
      "publishinggggggggg"
      ws.send(msg)
    end
  end

  class Coder < Connection
    attr_reader :tidal

    def initialize(name:, ws:, tidal:)
      super(name: name, ws: ws)
      @tidal = tidal
    end

    def process_message(message)
      tidal.evaluate message["data"]
    end
  end
end
