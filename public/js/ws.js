function WS(url) {
  var self = this;

  self.listeners = {
   open: [],
   message: [],
   close: [],
  };

  self.connection = null;

  self.addListener = function (type, handler) {
   self.listeners[type].push(handler)
  }

  self.onopen = function(event) {
   callListeners('open', event)
  };

  self.onerror = function (event) {
   console.error(event);
  };

  self.onclose = function (event) {
   callListeners('close', event)
   self.reconnect();
  };

  self.onmessage = function(event) {
   callListeners('message', event)
  };

  self.reconnect = function () {
   setTimeout(function() { self.connect(url) }, 1000);
  };

  self.connect = function() {
   self.connection = new WebSocket(url);
   self.connection.onopen = self.onopen;
   self.connection.onerror = self.onerror;
   self.connection.onclose = self.onclose;
   self.connection.onmessage = self.onmessage;
  };

  function callListeners(type, event) {
   self.listeners[type].forEach(function(handler) {
     try {
       handler(event)
     } catch (e) {
       console.error(e)
     }
   });
  }
}
