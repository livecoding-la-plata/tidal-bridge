# Tidal Bridge

Concentrator based on Hydra (https://github.com/ojack/hydra/) for tidal code that's written in browsers. 


The idea behind it was to have an easy way to set up a multi-laptop audio-visual performance with minimal technical infrastructure:

* a LAN
* a machine that outputs all the sound (through a single instance of superdirt)
* a machine that outputs hydra visuals (can be the same machine)

and allows:

* coders can write tidal code using the web editor 
* a single coder can write hydra code in hydra instance

* TODO: Allow multiple coders to write hydra code (we need to figure out how first :P)

### Usage

Needs an instance of SuperDirt running listening for Tidal messsages. 
The app doesn't enforce a way of running, as long as it's listening in the default port, it should work.

There's a helper script in `bin/tidal-server`, but's not mandatory to use it

#### Backend

Run the bin
```
$ ./bin/tidal-bridge
```

if you use `stack` you can pass as first argument:
```
$ ./bin/tidal-bridge 'stack ghci'
```

this will bring up a web-server for the editor and a websocket-server for the messages.
You only need to know the IP for sharing with the rest of computers.

There is also a bin for `hydra`
```
$ ./bin/hydra
```

this will run hydra and inside the messages recieved from the editors will be displayed
(with the name of each coder)

#### Hydra Frontend

Point you browser to `https://ip-of-the-server-machine:8000`. 

That's it! :party-parrot:

Take a look to hydra here: https://github.com/ojack/hydra
Or here for a functions reference: https://github.com/ojack/hydra/blob/master/docs/funcs.md

#### Tidal Frontend

Point you browser to `https://ip-of-the-server-machine:9292/#your-nick-name`

This will spawn a tidal process for you in the server machine.

To evaluate the line of code you have the pointer in, hit `Ctrl + Enter`.

The nick name you put after the `#` in the url will be shown in hydra when you evaluate code

You can chat hitting `Ctrl + Alt + Enter` and the selected text will be shown to all the coders.

There are a few commands to provide a little more interaction between coders.

Commands starts with the / character and right now these are available:

* `/help` - will show a simple help with the commands list
* `/coders` - will show a list of all the current conected coders
* `/spy CODER_NAME` - will show the current code written in CODER_NAME's editor
* `/mess CODER_NAME CODE` - will send CODE to the CODER_NAME's tidal process
* `/chat MESSAGE` - sends the MESSAGE to the global chat

To run a command, you have to select the text an hit `Ctrl + Alt + Enter` (equal as chat)

## Contributing

Changes and proposals are always welcome!

* For new features and feature changes, open an issue in the issue tracker to start a talk with the dev team.

* For small fixes and small changes, you can branch from the current master, and open a pull request. 

* We try to keep Git history as linear as possible, so always rebase your branch before merging. 

* Try to keep commits small & focused, and use meaningfull commit messages. <3




