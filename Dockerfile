FROM debian:sid
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y build-essential bison zlib1g-dev libyaml-dev libssl-dev libgdbm-dev libreadline-dev libncurses5-dev libffi-dev ruby ruby-dev nodejs npm cabal-install

RUN cabal update && cabal install tidal

RUN gem install bundler
COPY Gemfile* /tmp/
RUN cd /tmp && bundle install

RUN npm install -g npm
COPY ./hydra/package* /tmp/
RUN cd /tmp && npm install -g

WORKDIR /root/clic
